import 'dart:async';

import 'package:flutter/services.dart';

class FlutterPluginNfc {
  static const MethodChannel _channel =
      const MethodChannel('flutter_plugin_nfc');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
  
  static Future<String> get nfcTag async {
    final String tag = await _channel.invokeMethod('NfcRead');
    return tag;
  }

  static Future<void> get stop async {
    await _channel.invokeMethod('NfcStop');
  }
}
