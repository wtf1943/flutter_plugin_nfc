#import "FlutterPluginNfcPlugin.h"
#import <flutter_plugin_nfc/flutter_plugin_nfc-Swift.h>

@implementation FlutterPluginNfcPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterPluginNfcPlugin registerWithRegistrar:registrar];
}
@end
